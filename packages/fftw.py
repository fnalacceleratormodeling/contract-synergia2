#!/usr/bin/env python
from contractor import *

def need_fftw():
    return need_internal_version_library("fftw.h",
                                         '#if !defined(FFTW_HAS_WISDOM) || !defined(FFTW_HAS_FPRINT_PLAN)\nreturn 1;\n#endif\n')

fftw_internal = Option(local_root,"fftw_internal", need_fftw,int,
                       "Install internal version of fftw")

fftw_url = "http://www.fftw.org/fftw-2.1.5.tar.gz"

conf_args = "--enable-mpi --disable-fortran --disable-dependency-tracking --enable-shared"
if fftw_internal.get():
    fftw = Package(local_root,'fftw',
                   [Unpack(url=fftw_url),Configure(conf_args),Make(),
                    Install()])
    fftw.new_var('include_dir','%(root.install_dir)s/include')
    fftw.new_var('lib_dir','%(root.install_dir)s/lib')
else:
    fftw = External_package('fftw')
    fftw_include = Option(local_root,"fftw/include","/usr/include",str,"fftw include directory")
    # some 32 bit systems seem to have /usr/lib64 with no libraries so check
    # for the library explicitly.
    if os.path.exists('/usr/lib64/libfftw.so'):
        default_lib = '/usr/lib64'
    else:
        default_lib = '/usr/lib'
    fftw_lib = Option(local_root,"fftw/lib",default_lib,str,"fftw library directory")
    fftw.new_var('include_dir',fftw_include.get())
    fftw.new_var('lib_dir',fftw_lib.get())
