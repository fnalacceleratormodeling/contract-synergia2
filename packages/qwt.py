#!/usr/bin/env python
from contractor import *
from qt import qt

patch_cmd = '(sed "s^INSTALLBASE.*=.*/usr/local.*^INSTALLBASE=%(root.install_dir)s/qwt^" qwtconfig.pri >tmp&& mv tmp qwtconfig.pri)'

qwt_url = "http://home.fnal.gov/~amundson/synergia2/contractor-archives/qwt-5.0.2.tar.bz2"

qwt = Package(local_root,'qwt',(Unpack(url=qwt_url),
    Build_command('patch',patch_cmd),
    Qmake(),Make(),Install()),
              [qt])
