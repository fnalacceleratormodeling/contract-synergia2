#!/usr/bin/env python
from contractor import *

qt_internal = Option(local_root,"qt_internal",0,int,
                      "Build/use internal version of Qt")

if platform.is_macosx():
    qt_url = "ftp://ftp.trolltech.com/qt/source/qt-mac-free-3.3.7.tar.bz2"
else:
    qt_url = "ftp://ftp.trolltech.com/qt/source/qt-x11-free-3.3.7.tar.bz2"

platform_flag = ""
if platform.is_linux():
    if platform.is_x86_64():
        platform_flag = "-platform linux-g++-64"

if qt_internal.get():
    qt = Package(local_root,'qt',
                 (Unpack(url=qt_url),
                  Build_command('accept','(sed "s/read acceptance/acceptance=yes/" configure >tmp; mv tmp configure; chmod +x configure)'),
                  Configure(extra_args='-thread %s' % platform_flag),
                  Make(),Install()))

    os.environ['QTDIR'] = qt.get_var("build_dir")
    os.environ['LD_LIBRARY_PATH'] = os.path.join(\
        qt.get_var("build_dir"),'lib') \
        + ':' + os.environ['LD_LIBRARY_PATH']
else:
    qt = External_package('qt')
    
