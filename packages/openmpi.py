#!/usr/bin/env python
from contractor import *

def need_mpi():
    return need_internal_executable('mpicxx')

openmpi_internal = Option(local_root,"openmpi_internal",need_mpi,int,
                          "Install internal version of openmpi")

openmpi_url = "http://www.open-mpi.org/software/ompi/v1.4/downloads/openmpi-1.4.5.tar.bz2"

openmpi_configure_options = Option(local_root,"openmpi_configure_options",
                                   "", str, "extra configure options to pass to configure for openmpi (e.g. myrinet options)")

if openmpi_internal.get():
    # in openmpi 1.3, --enable-ltdl-convenience is no longer accepted as a
    # configure option.

    openmpi = Package(local_root,'openmpi',
                      [Unpack(url=openmpi_url),
                       Configure("--enable-orterun-prefix-by-default " + \
                                 openmpi_configure_options.get()),
                       Make(),
                       Install()],[])
else:
    openmpi = External_package('openmpi')
