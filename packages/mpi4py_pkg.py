#!/usr/bin/env python
from contractor import *

from python import python, python_internal
from openmpi import openmpi

def need_mpi4py():
    if python_internal.get():
        retval = 1
    else:
        retval = need_internal_version_python_module("mpi4py",[1,1])
    return retval

mpi4py_url = "http://mpi4py.googlecode.com/files/mpi4py-1.2.2.tar.gz"

mpi4py_internal = Option(local_root,"mpi4py_internal",need_mpi4py, int,
                      "Build/use internal version of mpi4py")

if mpi4py_internal.get():
    mpi4py = Package(local_root,'mpi4py',
                   [Unpack(url=mpi4py_url),Py_install()],
                   [python, openmpi])
else:
    mpi4py = External_package('mpi4py')
