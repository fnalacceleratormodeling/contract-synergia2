#!/usr/bin/env python
from contractor import *
from qt import qt

qutexmlrpc_url = "http://home.fnal.gov/~amundson/synergia2/contractor-archives/qutexmlrpc-0.1.tar.gz"

class Qxrinstall(Stage):
    def __init__(self):
        Stage.__init__(self,'qxrinstall')
        
    def build_method(self):
        os.chdir(self._var("build_dir"))
        includedir = os.path.join(self._var("root.install_dir"),"include",
                                  "qutexr")
        libdir = os.path.join(self._var("root.install_dir"),"lib")
        if not os.path.isdir(includedir):
            os.makedirs(includedir)
        if not os.path.isdir(libdir):
            os.makedirs(libdir)
        command = '(cp -r *.h %s && cp -r lib* %s)' % \
                  (includedir,libdir)
        system_or_die(command,self._log())
        os.chdir(self._var("root.base_dir"))

qutexmlrpc = Package(local_root,'qutexmlrpc',
                     (Unpack(url=qutexmlrpc_url),
                      Build_command('patch','(echo "QMAKE_CXXFLAGS += -fPIC" >> qutexr.pro)'),
                      Qmake(),Make(),Qxrinstall()),
                     [qt],
                     build_dir='qutexmlrpc/src')
