#!/usr/bin/env python
from contractor import *

from chef_libs import chef_libs
from python import python
from boost import boost
from qutexmlrpc import qutexmlrpc
from freeglut import freeglut
from qwt import qwt
from miniglib import miniglib
# EGS: chef_widget better not get called from the synergia2 contract!!!
#  otherwise, we will be in numarray hell.
#from numarray_pkg import numarray

# fix location of boost in cw
#fix_cw_cmd = "cd interpreter && sed 's/boost_python-mt/boost_python%s-mt/' interpreter.pro > interpreter.pro.tmp && cp interpreter.pro.tmp interpreter.pro" % vars["boost_lib_suffix"]

def config_pri_cmd():
    return '''cat > ../chef-config/config.pri <<EOF 
# config.pri
########################################################################

CHEF_INSTALL_TOP_DIR   =  \$(INSTALLDIR)
FNAL_INSTALL_TOP_DIR   =  \$(INSTALLDIR)
BOOST_INSTALL_TOP_DIR  =  \$(INSTALLDIR)
BOOST_VERSION          =  %(boost.version)s
BOOST_SUFFIX           =  %(boost.lib_suffix)s
# I only need PYTHON_INSTALL_TOP_DIR to construct PYTHON_INC and PYTHON_LIB
# which I will get from python.py directly
#PYTHON_INSTALL_TOP_DIR = 
#PYTHON_INSTALL_TOP_DIR =  \$(INSTALLDIR)
PYTHON_VERSION          = %(python.version)s
QWT_INSTALL_TOP_DIR    =  \$(INSTALLDIR)/qwt
QUTEXR_INSTALL_TOP_DIR =  \$(INSTALLDIR)

########################################################################

BOOST_INC       =  %(boost.include_dir)s
GLIB_INC        =  %(miniglib.inc)s
GLIBCONFIG_INC  =  %(miniglib.config_inc)s

# include numarray in PYTHON_INC as well
PYTHON_INC      =  %(python.include_dir)s %(numarray.numarray_inc)s
QUTEXR_INC      =  \$\${QUTEXR_INSTALL_TOP_DIR}/include
QUTEXR_INC      +=  \$\${QUTEXR_INSTALL_TOP_DIR}/include/qutexr

INCLUDEPATH     += \$\${FNAL_INSTALL_TOP_DIR}/include/beamline
INCLUDEPATH     += \$\${FNAL_INSTALL_TOP_DIR}/include/bmlfactory
INCLUDEPATH     += \$\${FNAL_INSTALL_TOP_DIR}/include/mxyzptlk
INCLUDEPATH     += \$\${FNAL_INSTALL_TOP_DIR}/include/basic_toolkit
INCLUDEPATH     += \$\${FNAL_INSTALL_TOP_DIR}/include/physics_toolkit
INCLUDEPATH     += \$\${FNAL_INSTALL_TOP_DIR}/include/gms
INCLUDEPATH     += \$\${BOOST_INC}
INCLUDEPATH     += \$\${GLIB_INC} \$\${GLIBCONFIG_INC}
INCLUDEPATH     += \$\$QUTEXR_INC 
INCLUDEPATH += ./include
INCLUDEPATH += ./src/ui

BOOST_LIBDIR   =    %(boost.lib_dir)s
QWT_LIBDIR     =    \$\${QWT_INSTALL_TOP_DIR}/lib
QWT_INCDIR =    \$\${QWT_INSTALL_TOP_DIR}/include
QWT_LIBNAME = qwt
GLIB_LIBDIR    =    %(miniglib.libdir)s
FNAL_LIBDIR    =    \$\${FNAL_INSTALL_TOP_DIR}/lib
CHEF_LIBDIR    =    \$\${CHEF_INSTALL_TOP_DIR}/lib
QUTEXR_LIBDIR  =    \$\${QUTEXR_INSTALL_TOP_DIR}/lib
PYTHON_LIBDIR  =    %(python.libdir)s

################################################################################
QMAKE_CXXFLAGS           += -pipe 

UI_DIR       =  src/ui
MOC_DIR      =  src/moc
OBJECTS_DIR  =  src/obj

DESTDIR = ./lib

target.path = \$\$CHEF_INSTALL_TOP_DIR/lib
INSTALLS += target

# libutil is needed for openpty/forkpty (glibc) on linux  
LIBS    += -lutil

unix:QMAKE_INCDIR_QT     = \$\${QWT_INCDIR} \$\$QMAKE_INCDIR_QT
unix:QMAKE_LIBDIR        = \$\${QWT_LIBDIR} \$\$QMAKE_LIBDIR 

unix:QMAKE_LFLAGS_SHAPP += -Wl,-rpath,\$\${QWT_LIBDIR} 
unix:QMAKE_LFLAGS_SHLIB += -Wl,-rpath,\$\${QWT_LIBDIR} 
EOF
'''

chef_widget = Package(local_root,'chef-widget',
                      [Build_command('config_pri',config_pri_cmd()),
                      #Build_command('fix_cw',fix_cw_cmd),
                       Qmake(),Make(),Install()],
# EGS chef_widget better not be called from the synergia2 contract.
#                      [python,chef_libs,numarray,qutexmlrpc,freeglut,qwt,
                      [python,chef_libs,qutexmlrpc,freeglut,qwt,
                       boost,python,miniglib],
                      src_dir=os.path.join(chef_libs.get_var("src_dir"),
                                           'widget_toolkit'))
os.environ['INSTALLDIR'] = local_root.get_var("install_dir")
