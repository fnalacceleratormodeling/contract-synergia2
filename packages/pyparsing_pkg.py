#!/usr/bin/env python
from contractor import *

from python import python

def need_pyparsing():
    return need_internal_version_python_module("pyparsing",[1,5])
pyparsing_url = "http://sourceforge.net/projects/pyparsing/files/pyparsing/pyparsing-1.5.5/pyparsing-1.5.5.tar.gz/download"

pyparsing_internal = Option(local_root,"pyparsing_internal",need_pyparsing, int,
                      "Build/use internal version of pyparsing")

if pyparsing_internal.get():
    pyparsing = Package(local_root,'pyparsing',
                   [Unpack(url=pyparsing_url),Py_install()],
                   [python])
else:
    pyparsing = External_package('pyparsing')
