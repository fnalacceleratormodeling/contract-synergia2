#!/usr/bin/env python
from contractor import *

from m4 import m4

def need_flex():
    return need_internal_version_executable("flex --version",[2,5,30])

flex_internal = Option(local_root,"flex_internal",need_flex,int,
                       "Install internal version of flex")

flex_url = "http://downloads.sourceforge.net/project/flex/flex/flex-2.5.35/flex-2.5.35.tar.bz2"


if flex_internal.get():
    flex = Package(local_root,'flex',
                   [Unpack(url=flex_url),Configure(),Make(),Install()],[m4])
else:
    flex = External_package('flex')
