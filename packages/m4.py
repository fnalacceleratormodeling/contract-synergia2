#!/usr/bin/env python
from contractor import *

def need_m4():
    return need_internal_version_executable("m4 --version",[1,4,6],
        '[0-9.][0-9.]+')

m4_internal = Option(local_root,"m4_internal",need_m4,int,
                       "Install internal version of m4")

m4_url = "http://ftp.gnu.org/gnu/m4/m4-1.4.13.tar.bz2"

if m4_internal.get():
    m4 = Package(local_root,'m4',
                   [Unpack(url=m4_url),Configure(),Make(),Install()])
else:
    m4 = External_package('m4')
