#!/usr/bin/env python
from contractor import *

from python import python

def need_nlopt():
    return need_internal_version_library("nlopt.h",
                                         '#if !defined(NLOPT_H)\nreturn 1;\n#endif\n')

nlopt_url = "http://ab-initio.mit.edu/nlopt/nlopt-2.3.tar.gz"

nlopt_internal = Option(local_root,"nlopt_internal", need_nlopt, int,
                       "Install internal version of nlopt")

conf_args = "--enable-shared PYTHON=python"

if nlopt_internal.get():
    nlopt = Package(local_root,'nlopt',
                    [Unpack(url=nlopt_url),Configure(conf_args),Make(),
                    Install()])
    nlopt.new_var('include_dir','%(root.install_dir)s/include')
    nlopt.new_var('lib_dir','%(root.install_dir)s/lib')
else:
    nlopt = External_package('nlopt')
    nlopt_include = Option(local_root,"nlopt/include","/usr/include",str,"NLOPT include directory")
    nlopt_lib = Option(local_root,"nlopt/lib",default_lib,str,"NLOPT library directory")
    nlopt.new_var('include_dir',nlopt_include.get())
    nlopt.new_var('lib_dir',nlopt_lib.get())
