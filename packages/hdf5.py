#!/usr/bin/env python
from contractor import *

from gxx import gxx
from zlib import zlib

def need_hdf5():
    return need_internal_version_library("hdf5.h",
                                         "if((H5_VERS_MAJOR!=1)||(H5_VERS_MINOR<8))return 1;")


hdf5_internal = Option(local_root,"hdf5_internal",need_hdf5,int,
                       "Install internal version of hdf5")

hdf5_url = "ftp://www.hdfgroup.org/HDF5/releases/hdf5-1.8.11/src/hdf5-1.8.11.tar.bz2"

configure_args = '--enable-cxx'

if hdf5_internal.get():
    hdf5 = Package(local_root,'hdf5',
                   [Unpack(url=hdf5_url),Configure(configure_args),
                    Make(),Install()],
                   [gxx,zlib])
else:
    hdf5 = External_package('hdf5')
