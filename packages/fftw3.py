#!/usr/bin/env python
from contractor import *
from openmpi import openmpi

def need_fftw3():
    return need_internal_version_library("fftw3-mpi.h",
                                         '#if !defined(FFTW3_H)\nreturn 1;\n#endif\n')


fftw3_internal = Option(local_root,"fftw3_internal",need_fftw3,int,
                       "Install internal version of fftw3")

fftw3_use_openmp = Option(local_root,"fftw3/openmp", 1, int,
                          "build OpenMP enabled FFTW3")
 
fftw3_url = "http://www.fftw.org/fftw-3.3.tar.gz"

conf_args = "--disable-fortran --disable-dependency-tracking --enable-shared --enable-mpi"
if fftw3_use_openmp.get():
    conf_args += " --enable-openmp"

if fftw3_internal.get():
    fftw3 = Package(local_root,'fftw3',
                   [Unpack(url=fftw3_url),Configure(conf_args),Make(),
                    Install()],
                    [openmpi])
    fftw3.new_var('include_dir','%(root.install_dir)s/include')
    fftw3.new_var('lib_dir','%(root.install_dir)s/lib')
else:
    fftw3 = External_package('fftw3')
    fftw3_include = Option(local_root,"fftw3/include","/usr/include",str,"FFTW3 include directory")
    # some 32 bit systems seem to have /usr/lib64 with no libraries so check
    # for the library explicitly.
    if os.path.exists('/usr/lib64/libfftw3.so'):
        default_lib = '/usr/lib64'
    else:
        default_lib = '/usr/lib'
    fftw3_lib = Option(local_root,"fftw3/lib",default_lib,str,"FFTW3 library directory")
    fftw3.new_var('include_dir',fftw3_include.get())
    fftw3.new_var('lib_dir',fftw3_lib.get())
