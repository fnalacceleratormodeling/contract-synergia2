#!/usr/bin/env python

from contractor import *

def need_eigen3():
    return need_internal_version_library("eigen3/Eigen/src/Core/util/Macros.h",
                                         "if (EIGEN_VERSION_AT_LEAST(3,0,0)) return 0; else return 1;")

eigen3_internal = Option(local_root,"eigen3_internal",need_eigen3,int,
                       "Install internal version of eigen3")

eigen3_url = "http://bitbucket.org/eigen/eigen/get/3.1.3.tar.bz2"

if eigen3_internal.get():
    eigen3 = Package(local_root,'eigen3',
                   [Unpack(url=eigen3_url),Cmake(),Make(),
                    Install()],[],build_dir='eigen3-build')
    eigen3.new_var('include_dir','%(root.install_dir)s/include/eigen3')
else:
    eigen3 = External_package('eigen3')
    eigen3_include = Option(local_root,"eigen3/include","/usr/include/eigen3",str,"Eigen3 include directory")
    eigen3.new_var('include_dir',eigen3_include.get())
