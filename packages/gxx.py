#!/usr/bin/env python
from contractor import *

gxx_internal = Option(local_root,"gxx_internal",0,int,
                      "Build/use internal version of g++")

gcc_url = "http://home.fnal.gov/~amundson/synergia2/contractor-archives/gcc-core-3.4.5.tar.bz2"
gxx_url = "http://home.fnal.gov/~amundson/synergia2/contractor-archives/gcc-g++-3.4.5.tar.bz2"

if gxx_internal.get():
    gxx = Package(local_root,'gxx',
                  (Unpack(pattern=["gcc-core-*","gcc-g++-*"],
                          url=[gcc_url,gxx_url]),
                   Configure(),Make(),Install()),
                  build_dir='gxx-build')
else:
    gxx = External_package('gxx')
    
