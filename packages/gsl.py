#!/usr/bin/env python
from contractor import *

def need_gsl():
    return need_internal_version_executable("gsl-config --version",[1,12])

gsl_internal = Option(local_root,"gsl_internal",need_gsl,int,
                       "Install internal version of gsl")

gsl_url = "ftp://ftp.gnu.org/gnu/gsl/gsl-1.14.tar.gz"

if gsl_internal.get():
    gsl = Package(local_root,'gsl',
                   [Unpack(url=gsl_url),Configure(),Make(),Install()])
else:
    gsl = External_package('gsl')
