#!/usr/bin/env python

import sys
import os
import commands
import string

from contractor import *

from bison import bison
from boost import boost
from cmake import cmake
from fftw3 import fftw3
from flex import flex
from gxx import gxx
from miniglib import miniglib
from numpy_pkg import numpy, numpy_internal
from python import python

chef_checkout = Option(local_root, "chef-libs/checkout", 1, int,
                    "Check Chef out of repository instead of using tarball")
chef_branch = Option(local_root, "chef-libs/branch",
                     'StRel20080125-patches-cmake', str,
                     "Branch to use when checking out Chef from repository")
chef_build_parsers = Option(local_root, "chef-libs/build_parsers", 1, int,
                            "Build CHEF parsers")
chef_build_pybind = Option(local_root, "chef-libs/build_pybindings", 1, int,
                            "Build python bindings for CHEF")
chef_build_static = Option(local_root, "chef-libs/static_libs", 0, int,
                            "Build CHEF libraries as static instead of shared")

if chef_checkout.get():
    get_chef = Git_clone('http://cdcvs.fnal.gov/projects/accelerator-modeling-chef',
                         branch=chef_branch.get())
else:
    get_chef = Unpack()

chef_libs_conf = " -DPYTHON_INCLUDE_PATH:PATH=%(python.include_dir)s"
chef_libs_conf += " -DPYTHON_LIBRARY:FILEPATH=-lpython%(python.version)s"
chef_libs_conf += ' -DBOOST_INCLUDEDIR:PATH=%(boost.include_dir)s'
chef_libs_conf += ' -DFFTW3_LIBRARY_DIRS="%(fftw3.lib_dir)s"'
chef_libs_conf += ' -DGLIB_INC:PATH="%(miniglib.inc)s"'
chef_libs_conf += ' -DGLIBCONFIG_INC:PATH="%(miniglib.config_inc)s"'
chef_libs_conf += ' -DGLIB_LIB:PATH="%(miniglib.libdir)s"'
chef_libs_conf += ' -DCMAKE_BUILD_TYPE=Release'
chef_libs_conf += (' -DBUILD_PARSER_MODULES=%(build_parsers)u' +
                   ' -DBUILD_PYTHON_BINDINGS=%(build_python)u' +
                   ' -DBUILD_SHARED_LIBS=%(build_static)u') % \
                   { "build_parsers": chef_build_parsers.get(),
                     "build_python": chef_build_pybind.get(),
                     "build_static": (not chef_build_static.get()) }

deps = [python, cmake, boost, gxx, fftw3, numpy, miniglib]
if (chef_build_parsers.get()):
  deps.extend([ flex, bison ])

chef_libs = Package(local_root, 'chef-libs',
                     [get_chef,
                      Cmake(extra_args=chef_libs_conf),
                      Make(),
                      Install()],
                     deps)

os.environ['INSTALLDIR'] = chef_libs.get_var("root.install_dir")
