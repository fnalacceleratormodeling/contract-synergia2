#!/usr/bin/env python
from contractor import *

freeglut_url = "http://home.fnal.gov/~amundson/synergia2/contractor-archives/freeglut-2.4.0.tar.gz"

freeglut = Package(local_root,'freeglut',(Unpack(url=freeglut_url),
                                          Configure(extra_args="--enable-warnings=no")
                                          ,Make(),Install()))
