#!/usr/bin/env python
from contractor import *

def need_freetype():
    return need_internal_executable("freetype-config")
    
freetype_internal = Option(local_root,"freetype_internal",need_freetype,int,
                       "Install internal version of freetype")

freetype_url = "http://downloads.sourceforge.net/project/freetype/freetype2/2.3.9/freetype-2.3.9.tar.bz2"

if freetype_internal.get():
    freetype = Package(local_root,'freetype',
                   [Unpack(url=freetype_url),Configure(),Make(),
                    Install()])
else:
    freetype = External_package('freetype')
    
