#!/usr/bin/env python
from contractor import *

def need_cmake():
    return need_internal_version_executable("cmake --version",[2,8])

cmake_internal = Option(local_root,"cmake_internal",need_cmake,int,
                       "Install internal version of cmake")

cmake_url = "http://www.cmake.org/files/v2.8/cmake-2.8.10.2.tar.gz"

if cmake_internal.get():
    cmake = Package(local_root,'cmake',
                   [Unpack(url=cmake_url),Configure(),Make(),Install()])
else:
    cmake = External_package('cmake')
