#!/usr/bin/env python
from contractor import *

from chef_libs import chef_libs
from chef_widget import chef_widget
from freeglut import freeglut

chef_gui = Package(local_root,'chef-gui',
                [Qmake(),Make(),Install(),
                    Build_command('move_to_bin',
                        'mv %(root.install_dir)s/lib/chef %(root.install_dir)s/bin')],
                [chef_libs,chef_widget,freeglut],
                src_dir=os.path.join(chef_libs.get_var("src_dir"),
                    'chef-mdi'))
