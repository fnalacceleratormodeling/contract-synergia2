#!/usr/bin/env python
from contractor import *

from python import python

numeric_url = "http://home.fnal.gov/~amundson/synergia2/contractor-archives/Numeric-24.2.tar.gz"

numeric = Package(local_root,'numeric',
                  (Unpack(pattern="Numeric-*",url=numeric_url),
                   Py_install()),[python])
