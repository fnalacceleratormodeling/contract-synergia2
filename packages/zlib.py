#!/usr/bin/env python
from contractor import *

def need_zlib():
    return need_internal_version_library("zlib.h")
    
zlib_internal = Option(local_root,"zlib_internal",need_zlib,int,
                       "Install internal version of zlib")

zlib_url = "http://prdownloads.sourceforge.net/libpng/zlib-1.2.3.tar.bz2?download"

if zlib_internal.get():
    zlib = Package(local_root,'zlib',
                   [Unpack(url=zlib_url),Configure('--shared'),Make(),
                    Install()])
else:
    zlib = External_package('zlib')
    
