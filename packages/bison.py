#!/usr/bin/env python
from contractor import *
from m4 import m4

def need_bison():
    return need_internal_version_executable("bison --version",[2,4,1])

bison_internal = Option(local_root,"bison_internal",need_bison,int,
                       "Install internal version of bison")

bison_url = "http://ftp.gnu.org/gnu/bison/bison-2.4.1.tar.bz2"

if bison_internal.get():
    bison = Package(local_root,'bison',
                   [Unpack(url=bison_url),Configure(),Make(),Install()],[m4])
else:
    bison = External_package('bison')
