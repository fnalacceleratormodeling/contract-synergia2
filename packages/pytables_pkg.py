#!/usr/bin/env python
from contractor import *

from python import python
from numpy_pkg import numpy
from hdf5 import hdf5,hdf5_internal

pytables_url = "https://compacc.fnal.gov/projects/attachments/download/20/tables-2.1.2.tar.gz"

if hdf5_internal.get():
    pytables_args = ' --hdf5="%(root.install_dir)s"'
else:
    pytables_args = ''
    
pytables = Package(local_root,'pytables',
                   [Unpack(pattern='tables*',url=pytables_url),Py_install(pytables_args)],
                   [python,numpy,hdf5])

